/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestao_fibras_artificiais.domain;

import java.math.BigDecimal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Emanuel Marques
 */
public class FiberFamilyTest {
    
    public FiberFamilyTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

//    /**
//     * Test of calcCustoMateriais method, of class FiberFamily.
//     */
//    @org.junit.Test
//    public void testCalcCustoMateriais() {
//        System.out.println("calcCustoMateriais");
//        int qnt = 0;
//        FiberFamily instance = null;
//        BigDecimal expResult = null;
//        BigDecimal result = instance.calcCustoMateriais(qnt);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    
    /**
     * Test of creating a new Pedido de Orçamento.
     */
    @org.junit.Test (expected = IllegalArgumentException.class)
    public void testeLancamentoExcecaoNaCriaçaoDeUmaFamiliaDeFibra() {
        System.out.println("Teste de lançamento de exceção na criação de uma familia de fibra\n");
        FiberFamily instance = new FiberFamily("sigla", "nome", BigDecimal.ZERO);
    }
    
}
