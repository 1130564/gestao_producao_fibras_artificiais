/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestao_fibras_artificiais.domain;

import java.math.BigDecimal;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Emanuel Marques
 */
public class PedidoDeOrçamentoTest {
    
    public PedidoDeOrçamentoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

//    /**
//     * Test of getQuantidade method, of class PedidoDeOrçamento.
//     */
//    @org.junit.Test
//    public void testGetQuantidade() {
//        System.out.println("getQuantidade");
//        PedidoDeOrçamento instance = null;
//        int expResult = 0;
//        int result = instance.getQuantidade();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getFamiliaDeFibra method, of class PedidoDeOrçamento.
//     */
//    @org.junit.Test
//    public void testGetFamiliaDeFibra() {
//        System.out.println("getFamiliaDeFibra");
//        PedidoDeOrçamento instance = null;
//        FiberFamily expResult = null;
//        FiberFamily result = instance.getFamiliaDeFibra();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getTipoDeCor method, of class PedidoDeOrçamento.
//     */
//    @org.junit.Test
//    public void testGetTipoDeCor() {
//        System.out.println("getTipoDeCor");
//        PedidoDeOrçamento instance = null;
//        TipoDeCor expResult = null;
//        TipoDeCor result = instance.getTipoDeCor();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of calcCustoMateriais method, of class PedidoDeOrçamento.
//     */
//    @org.junit.Test
//    public void testCalcCustoMateriais() {
//        System.out.println("calcCustoMateriais");
//        PedidoDeOrçamento instance = null;
//        BigDecimal expResult = null;
//        BigDecimal result = instance.calcCustoMateriais();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of toString method, of class PedidoDeOrçamento.
//     */
//    @org.junit.Test
//    public void testToString() {
//        System.out.println("toString");
//        PedidoDeOrçamento instance = null;
//        String expResult = "";
//        String result = instance.toString();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    
    /**
     * Test of creating a new Pedido de Orçamento.
     */
    @org.junit.Test (expected = IllegalArgumentException.class)
    public void testeLancamentoExcecaoNaCriaçaoDeUmPedidoDeOrçamento() {
        System.out.println("Teste de lançamento de exceção na criação de um pedido de orçamento\n");
        FiberFamily ff = new FiberFamily("sigla", "nome", BigDecimal.ZERO);
        TipoDeCor cor = new TipoDeCor("descricao");
        int qnt = 0;
        PedidoDeOrçamento instance = new PedidoDeOrçamento(qnt, ff, cor);
    }
    
}
