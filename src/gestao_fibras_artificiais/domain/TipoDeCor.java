/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestao_fibras_artificiais.domain;

import java.math.BigDecimal;

/**
 *
 * @author Emanuel Marques
 */
public class TipoDeCor {

    /**
     * descriçao do tipo de cor
     */
    private final String descriçao;

    

    /**
     * Constroi um tipo de cor a partir da sua descriçao e custo de tinturaria
     *
     * @param descriçao
     */
    public TipoDeCor(String descriçao) {
        this.descriçao = descriçao;
      
        
    }

    @Override
    public String toString() {
        return "Descricao: " + descriçao;
    }

 

}
