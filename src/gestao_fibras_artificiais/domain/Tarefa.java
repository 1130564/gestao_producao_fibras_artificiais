/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestao_fibras_artificiais.domain;

import java.math.BigDecimal;

/**
 *
 * @author Emanuel Marques
 */
public abstract class Tarefa {

    /**
     * designaçao da tarefa
     */
    private final String designaçao;
    
    /**
     * Constroi uma tarefa base
     *
     * @param designaçao
     */
    public Tarefa(String designaçao) {
        this.designaçao = designaçao;
    }
    public Tarefa(){
        
        this.designaçao = "default";
    }

  
    /**
     * @param pedido
     * @return o custa da tarefa
     */
    public abstract BigDecimal getCustoTarefa(PedidoDeOrçamento pedido);

}
