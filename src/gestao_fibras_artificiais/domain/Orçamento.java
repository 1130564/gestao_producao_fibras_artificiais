/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestao_fibras_artificiais.domain;

import java.math.BigDecimal;

/**
 *
 * @author vitor_000
 */
public class Orçamento {

    private final PedidoDeOrçamento pedidoOrc;
    private BigDecimal custoTotal;
    private BigDecimal custoMateriais;
    private BigDecimal custoProducao;

    public Orçamento(PedidoDeOrçamento pedido) {
        if(pedido == null) {
            throw new IllegalArgumentException("Pedido nulo!\n");
        } else {
            this.pedidoOrc = pedido;
            this.custoTotal = BigDecimal.ZERO;
        }
    }
    
    public void calcula() {
        
        this.calcCustoMateriais();
        this.calcCustoProducao();
        
        this.custoTotal = this.custoTotal.add(this.custoMateriais);
        this.custoTotal = this.custoTotal.add(this.custoProducao);
        
    }
    
    private void calcCustoMateriais() {
        this.custoMateriais = this.pedidoOrc.calcCustoMateriais();
    }
    
    private void calcCustoProducao() {
        ProcessoProdutivo processo = new ProcessoProdutivo();
        this.custoProducao = processo.calculaCusto(pedidoOrc);
    }

    @Override
    public String toString() {
        return "Orçamento:\n" + "Pedido: " + this.pedidoOrc.toString() + "\nCusto Total: " + custoTotal + "\nCusto Materiais: " + custoMateriais + "\nCusto Produção: " + custoProducao;
    }
    
    

}
