/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestao_fibras_artificiais.domain;

import gestao_fibras_artificiais.repository.InMemoryTarefaRepository;
import gestao_fibras_artificiais.repository.TarefaRepository;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author vitor_000
 */
public class ProcessoProdutivo {
    
    public ProcessoProdutivo() {
        
    }
    
    public BigDecimal calculaCusto(PedidoDeOrçamento pedido) {
        TarefaRepository repository = new InMemoryTarefaRepository();
        List<Tarefa> tarefas = repository.all();
        BigDecimal total = BigDecimal.ZERO;
        
        for(Tarefa t : tarefas) {
            total = total.add(t.getCustoTarefa(pedido));
        }
        
        return total;
    }
}
