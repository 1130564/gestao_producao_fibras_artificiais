/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestao_fibras_artificiais.domain;

import java.math.BigDecimal;

/**
 *
 * @author vitor_000
 */
public class TarefaPorKg extends Tarefa {
    
    private BigDecimal preco;
    
    public TarefaPorKg(String designaçao, BigDecimal preco) {
        super(designaçao);
        this.preco = preco;
    }

    @Override
    public BigDecimal getCustoTarefa(PedidoDeOrçamento pedido) {
        return preco.multiply(new BigDecimal(pedido.getQuantidade()));
    }
    
}
