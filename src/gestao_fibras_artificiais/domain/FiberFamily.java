/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestao_fibras_artificiais.domain;

import java.math.BigDecimal;

/**
 *
 * @author Emanuel Marques
 */
public class FiberFamily {

    private String sigla;
    private String nome;
    private BigDecimal custoKg;
    
    public FiberFamily(String sigla, String nome, BigDecimal custoKg) {
        if (sigla.isEmpty() || nome.isEmpty() || custoKg == null || custoKg.compareTo(BigDecimal.ZERO) == 0) {
            throw new IllegalArgumentException("Parametros invalidos!\n");
        } else {
            this.sigla = sigla;
            this.nome = nome;
            this.custoKg = custoKg;
        }
    }

    public BigDecimal calcCustoMateriais(int qnt) {
        return this.custoKg.multiply(new BigDecimal(qnt));
    }

    @Override
    public String toString() {
        return "Sigla: " + sigla + " Nome: " + nome + " CustoKg: " + custoKg;
    }

}
