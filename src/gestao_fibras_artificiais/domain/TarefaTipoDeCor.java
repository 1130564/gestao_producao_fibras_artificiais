/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestao_fibras_artificiais.domain;

import java.math.BigDecimal;
import java.util.Map;

/**
 *
 * @author vitor_000
 */
public class TarefaTipoDeCor extends Tarefa {

    private Map<TipoDeCor, BigDecimal> tabelaPrecos;
    
    public TarefaTipoDeCor(String designaçao, Map<TipoDeCor, BigDecimal> tabela) {
        super(designaçao);
        this.tabelaPrecos = tabela;
        
    }

    /**
     *
     */
    public TarefaTipoDeCor() {
       super();
    }

    @Override
    public BigDecimal getCustoTarefa(PedidoDeOrçamento pedido) {
        return (tabelaPrecos.containsKey(pedido.getTipoDeCor())) ? tabelaPrecos.get(pedido.getTipoDeCor()) : new BigDecimal(0);
    }

    public Map<TipoDeCor, BigDecimal> getTabelaPrecos() {
        return tabelaPrecos;
    }
    
    
    
}
