/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestao_fibras_artificiais.domain;

import java.math.BigDecimal;

/**
 *
 * @author Emanuel Marques
 */
public class PedidoDeOrçamento {

    /**
     * quantidade da fibra
     */
    private final int quantidade;
    /**
     * Familia da fibra
     */
    private final FiberFamily familiaDeFibra;
    /**
     * Tipo de Cor
     */
    private final TipoDeCor tipoDeCor;

    /**
     * Cria um pedido de orçamento com todos os parametros
     *
     * @param quantidade quantidade de fibra a produzir
     * @param familiaDeFibra familia de fibra desejada
     * @param tipoDeCor tipo de cor da fibra
     */
    public PedidoDeOrçamento(int quantidade, FiberFamily familiaDeFibra, TipoDeCor tipoDeCor) {
        if (quantidade <= 0) {
            throw new IllegalArgumentException("Quantidade tem de ser superior a 0!");
        } else {
            this.quantidade = quantidade;
            this.familiaDeFibra = familiaDeFibra;
            this.tipoDeCor = tipoDeCor;
        }

    }

    /**
     * @return the quantidade
     */
    public int getQuantidade() {
        return quantidade;
    }

    /**
     * @return the familiaDeFibra
     */
    public FiberFamily getFamiliaDeFibra() {
        return familiaDeFibra;
    }

    /**
     * @return the tipoDeCor
     */
    public TipoDeCor getTipoDeCor() {
        return tipoDeCor;
    }

    public BigDecimal calcCustoMateriais() {
        return this.familiaDeFibra.calcCustoMateriais(this.quantidade);
    }

    @Override
    public String toString() {
        return "Pedido de Orçamento:\n" + "Quantidade: " + quantidade + "\nFamilia de Fibra: " + familiaDeFibra + "\nTipo de Cor: " + tipoDeCor;
    }

}
