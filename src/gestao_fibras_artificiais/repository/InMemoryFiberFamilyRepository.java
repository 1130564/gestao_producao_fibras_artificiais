/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestao_fibras_artificiais.repository;

import gestao_fibras_artificiais.domain.FiberFamily;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Paulo Gandra de Sousa
 */
public class InMemoryFiberFamilyRepository implements FiberFamilyRepository {

    // static data for demo purposes
    private static final List<FiberFamily> data = new ArrayList<>();

    // static initializer for demo purposes
    static {
        data.add(new FiberFamily("PA", "Poliamida", new BigDecimal(0.85)));
        data.add(new FiberFamily("PES", "Poliester", new BigDecimal(0.80)));
        data.add(new FiberFamily("SP", "Spandex", new BigDecimal(0.75)));
        data.add(new FiberFamily("VI", "Viscose", new BigDecimal(0.70)));
    }

    @Override
    public List<FiberFamily> all() {
        return Collections.unmodifiableList(data);
    }
}