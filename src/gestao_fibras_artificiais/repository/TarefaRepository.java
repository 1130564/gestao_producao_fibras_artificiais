/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestao_fibras_artificiais.repository;

import gestao_fibras_artificiais.domain.Tarefa;
import java.util.List;

/**
 *
 * @author Emanuel Marques
 */
public interface TarefaRepository {
    List<Tarefa> all();
}
