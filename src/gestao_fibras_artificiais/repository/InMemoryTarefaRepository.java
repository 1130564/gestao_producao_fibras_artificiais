/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestao_fibras_artificiais.repository;

import gestao_fibras_artificiais.domain.Tarefa;
import gestao_fibras_artificiais.domain.TarefaPorKg;
import gestao_fibras_artificiais.domain.TarefaTipoDeCor;
import gestao_fibras_artificiais.domain.TipoDeCor;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Emanuel Marques
 */
public class InMemoryTarefaRepository implements TarefaRepository {

    // static data for demo purposes
    private static final List<Tarefa> data = new ArrayList<>();

    // static initializer for demo purposes
    static {
        Map<TipoDeCor, BigDecimal> tabelaPreçosTinturaria = new HashMap<>();
        tabelaPreçosTinturaria.put(new TipoDeCor("Cores Claras"), new BigDecimal(0.80));
        tabelaPreçosTinturaria.put(new TipoDeCor("Cores Escuras"), new BigDecimal(1.10));
        tabelaPreçosTinturaria.put(new TipoDeCor("Cores Intensas"), new BigDecimal(1.35));

        //inicializar os preços
        data.add(new TarefaPorKg("Extrusao", new BigDecimal(0.3)));
        data.add(new TarefaPorKg("Texturizaçao", new BigDecimal(1.1)));
        data.add(new TarefaPorKg("Torcedura", new BigDecimal(0.25)));
        data.add(new TarefaTipoDeCor("Tinturaria", tabelaPreçosTinturaria));
        data.add(new TarefaPorKg("Bobinagem", new BigDecimal(0.20)));
        data.add(new TarefaPorKg("Revista", new BigDecimal(0.1)));
    }

    @Override
    public List<Tarefa> all() {
        return Collections.unmodifiableList(data);
    }

}
