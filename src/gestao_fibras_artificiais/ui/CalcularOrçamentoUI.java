/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestao_fibras_artificiais.ui;

import gestao_fibras_artificiais.controller.CalcularOrçamentoController;
import gestao_fibras_artificiais.domain.FiberFamily;
import gestao_fibras_artificiais.domain.Orçamento;
import gestao_fibras_artificiais.domain.TipoDeCor;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author vitor_000
 */
public class CalcularOrçamentoUI {

    private final CalcularOrçamentoController controllerCO;
    private Scanner in;

    public CalcularOrçamentoUI() {
        this.controllerCO = new CalcularOrçamentoController();
        this.in = new Scanner(System.in);
    }

    public void run() {
        boolean flag;
        int indice;
        FiberFamily fibra = new FiberFamily("nova", "nova", BigDecimal.ONE);
        TipoDeCor cor = new TipoDeCor("desc");

        do {
            try {
                flag = true;
                System.out.println("Escolha uma fibra das famílias seguintes:\n");
                List<FiberFamily> listaFibras = this.controllerCO.getFamiliasFibra();
                apresentarFamiliasFibra(listaFibras);
                indice = this.in.nextInt();
                fibra = listaFibras.get(indice - 1);

            } catch (ArrayIndexOutOfBoundsException ex) {
                ex.toString();
                flag = false;
            }
        } while (!flag);

        do {
            try {
                flag = true;
                System.out.println("Escolha uma cor das cores seguintes:\n");
                List<TipoDeCor> listaCor = this.controllerCO.getTiposCor();
                apresentarTiposDeCor(listaCor);
                indice = this.in.nextInt();
                cor = listaCor.get(indice - 1);

            } catch (ArrayIndexOutOfBoundsException ex) {
                ex.toString();
                flag = false;
            }
        } while (!flag);

        do {
            try {
                flag = true;
                int qnt;
                System.out.println("Introduza uma quantidade:\n");
                qnt = this.in.nextInt();

                this.controllerCO.calculaOrçamento(fibra, cor, qnt);
                System.out.println(this.controllerCO.apresentarOrçamento());

            } catch (IllegalArgumentException ex) {
                ex.toString();
                flag = false;
            }
        } while (!flag);

    }

    private void apresentarFamiliasFibra(List<FiberFamily> lista) {
        int i = 1;
        for (FiberFamily fibra : lista) {
            System.out.println(i + ": " + fibra.toString() + "\n");
            i++;
        }
    }

    private void apresentarTiposDeCor(List<TipoDeCor> lista) {
        int i = 1;
        for (TipoDeCor cor : lista) {
            System.out.println(i + ": " + cor.toString() + "\n");
            i++;
        }
    }

}
