/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gestao_fibras_artificiais.controller;

import gestao_fibras_artificiais.domain.FiberFamily;
import gestao_fibras_artificiais.domain.Orçamento;
import gestao_fibras_artificiais.domain.PedidoDeOrçamento;
import gestao_fibras_artificiais.domain.Tarefa;
import gestao_fibras_artificiais.domain.TarefaTipoDeCor;
import gestao_fibras_artificiais.domain.TipoDeCor;
import gestao_fibras_artificiais.repository.FiberFamilyRepository;
import gestao_fibras_artificiais.repository.InMemoryFiberFamilyRepository;
import gestao_fibras_artificiais.repository.InMemoryTarefaRepository;
import gestao_fibras_artificiais.repository.TarefaRepository;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Emanuel Marques
 */
public class CalcularOrçamentoController {
    
    private Orçamento orçamento;
    
    
    /**
     * instancia o controlador
     */
    public CalcularOrçamentoController() {
        
    }
    
    /**
     * Retorna as familias de fibra disponiveis
     * @return familias de fibra disponiveis
     */
    public List<FiberFamily> getFamiliasFibra(){
        FiberFamilyRepository familyRepo = new InMemoryFiberFamilyRepository();
        
        return familyRepo.all();
        
    }
    
    /**
     * Retorna os tipos de cor disponiveis
     * @return tipos de cor disponiveis
     */
    public List <TipoDeCor> getTiposCor(){
        TarefaRepository tarefaRepo = new InMemoryTarefaRepository();
        
        List<Tarefa> listaTarefas = tarefaRepo.all();
        
        Tarefa tinturaria = new TarefaTipoDeCor();
        for (Tarefa tarefa : listaTarefas) {
            if (tarefa instanceof TarefaTipoDeCor){
                tinturaria = tarefa;
                
            }
            
        }
        
        Map<TipoDeCor, BigDecimal> tabelaPrecos =((TarefaTipoDeCor)tinturaria).getTabelaPrecos();
        
        List<TipoDeCor> tiposDeCor = new ArrayList<>();
               
        
        for (Map.Entry<TipoDeCor, BigDecimal> entrySet : tabelaPrecos.entrySet()) {
            TipoDeCor key = entrySet.getKey();
            tiposDeCor.add(key);
            
            
        }
        return tiposDeCor;
    }
    
    /**
     * Retorna os dados do pedido
     * @return dados do pedido de orçamento
     */
//    public String getDados(){
//        return pedido.toString();
//    }
    
    /**
     * Calcula o orçamento para o pedido efetuado
     * @param fibra
     * @param cor
     * @param qnt
     */
    public void calculaOrçamento(FiberFamily fibra, TipoDeCor cor, int qnt){
        
        PedidoDeOrçamento pedido = new PedidoDeOrçamento(qnt, fibra, cor);
        this.orçamento = new Orçamento(pedido);
        this.orçamento.calcula();
        
    }
    
    public String apresentarOrçamento() {
        return this.orçamento.toString();
    }
    
    
    
}
